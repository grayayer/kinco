<?php

// wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
//function my_script() {
	if (!is_admin()) {
		wp_enqueue_script('jquery');
		wp_enqueue_script('superfish', get_bloginfo('template_url').'/js/superfish.js', array('jquery'), '1.4.8', false);
		wp_enqueue_script('faded', get_bloginfo('template_url').'/js/jquery.faded.js', array('jquery'), '0.3.2', false);
		wp_enqueue_script('jcarousellite', get_bloginfo('template_url').'/js/jcarousellite.js', array('jquery'), '0.3.2', false);
        wp_enqueue_script('jquerycookie', get_bloginfo('template_url').'/js/jquery.cookie.js', array('jquery'), '1.4.1', false);
	}
//}
//add_action('init', 'my_script');
?>