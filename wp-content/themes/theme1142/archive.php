<?php get_header(); ?>
<div id="primary">
   <div class="inside"><h1 class="title"><?php if ( is_day() ) : ?><!-- if the daily archive is loaded --><?php printf( __( 'Daily Archives: <span>%s</span>' ), get_the_date() ); ?><?php elseif ( is_month() ) : ?><!-- if the montly archive is loaded --><?php printf( __( 'Monthly Archives: <span>%s</span>' ), get_the_date('F Y') ); ?><?php elseif ( is_year() ) : ?><!-- if the yearly archive is loaded --><?php printf( __( 'Yearly Archives: <span>%s</span>' ), get_the_date('Y') ); ?>
      <?php else : ?>
      <!-- if anything else is loaded, ex. if the tags or categories template is missing this page will load -->
      Blog Archives
      <?php endif; ?></h1>
   <ul class="posts-list">
   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   <li>
   <div class="title"><h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2> </div>
   <?php if ( has_post_thumbnail()) { echo '<div class="featuredThumbnail">'; the_post_thumbnail(); echo '</div>'; } ?>
   <!-- loades the post's featured thumbnail, requires Wordpress 3.0+ -->
   <div class="post-excerpt">
      <?php the_excerpt(); ?>
      <!-- the excerpt is loaded to help avoid duplicate content issues -->
   </div>
   <div class="post-meta">
         Categories:
         <?php the_category(', ') ?>
         <br />
         <?php if (the_tags('Tags: ', ', ', ' ')); ?>
   </div>
   <!--.postMeta-->
   </li>
   <?php endwhile; else: ?>
   <div class="no-results">
      <p><strong>There has been an error.</strong></p>
      <p>We apologize for any inconvenience, please <a href="<?php bloginfo('url'); ?>/" title="<?php bloginfo('description'); ?>">return to the home page</a> or use the search form below.</p>
      <?php get_search_form(); ?>
      <!-- outputs the default Wordpress search form-->
   </div>
   <!--noResults-->
   <?php endif; ?>
   </ul>
   <div class="newer-older bot">
      <div class="older">
         <p>
            <?php next_posts_link('&laquo; Older Entries') ?>
         </p>
      </div>
      <!--.older-->
      <div class="newer">
         <p>
            <?php previous_posts_link('Newer Entries &raquo;') ?>
         </p>
      </div>
      <!--.older-->
   </div>
   <!--.oldernewer--></div>
</div>
<!--#content-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>