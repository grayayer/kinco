<?php
  // enables wigitized sidebars
  if ( function_exists('register_sidebar') ) {
    // Sidebar Widget
    // Location: the sidebar
    register_sidebar(array('name'=>'Sidebar',
      'before_widget' => '<li id="%1$s" class="widget %2$s">',
      'after_widget' => '</li>',
      'before_title' => '<h3>',
      'after_title' => '</h3>',
    ));
    // Header Widget
    // Location: right after the navigation
    register_sidebar(array('name'=>'Header',
      'before_widget' => '<li id="%1$s" class="widget %2$s">',
      'after_widget' => '</li>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    // Footer Widget
    // Location: at the top of the footer, above the copyright
    register_sidebar(array('name'=>'Footer',
      'before_widget' => '<li id="%1$s" class="widget %2$s">',
      'after_widget' => '</li>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    // The Alert Widget
    // Location: displayed on the top of the home page, right after the header, right before the loop, within the contend area
    register_sidebar(array('name'=>'Alert',
      'before_widget' => '<li id="%1$s" class="widget %2$s">',
      'after_widget' => '</li>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    // Sidebar Widget
    // Location: the content
    register_sidebar(array('name'=>'Text sidebar',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h3>',
      'after_title' => '</h3>',
    ));
  }
  // post thumbnail support
  add_theme_support( 'post-thumbnails' );
  // custom menu support
  add_theme_support( 'menus' );
  if ( function_exists( 'register_nav_menus' ) ) {
      register_nav_menus(
        array(
          'header_menu' => 'Header Menu',
          'sidebar_menu' => 'Sidebar Menu',
          'footer_menu' => 'Footer Menu',
          'materials' => 'Materials 3rd Level',
          'brands' => 'Brand Details 3rd Level',
          'prod_detail' => 'Product Detail 3rd Level',
          'your_account_menu' => 'Your Account 3rd Level',
          'press_and_reviews_menu' => 'Press & Reviews 3rd Level'
        )
      );
  }

  add_theme_support( 'woocommerce' );

  // custom background support
  add_custom_background();
  // removes detailed login error information for security
  add_filter('login_errors',create_function('$a', "return null;"));
  // Removes Trackbacks from the comment cout
  add_filter('get_comments_number', 'comment_count', 0);
  function comment_count( $count ) {
    if ( ! is_admin() ) {
      global $id;
      $comments_by_type = &separate_comments(get_comments('status=approve&post_id=' . $id));
      return count($comments_by_type['comment']);
    } else {
      return $count;
    }
  }
  // custom excerpt ellipses for 2.9+
  function custom_excerpt_more($more) {
    return 'Read More &raquo;';
  }
  add_filter('excerpt_more', 'custom_excerpt_more');
  // no more jumping for read more link
  function no_more_jumping($post) {
    return ' <a href="'.get_permalink($post->ID).'" class="read-more">'.'more info'.'</a>';
  }
  add_filter('excerpt_more', 'no_more_jumping');
  // category id in body and post class
  function category_id_class($classes) {
    global $post;
    foreach((get_the_category($post->ID)) as $category)
      $classes [] = 'cat-' . $category->cat_ID . '-id';
      return $classes;
  }
  add_filter('post_class', 'category_id_class');
  add_filter('body_class', 'category_id_class');
  // Faded Slider
  $labels = array(
    'name' => _x('Slider', 'post type general name'),
    'singular_name' => _x('slides', 'post type singular name'),
    'add_new' => _x('Add New', 'faqs'),
    'add_new_item' => __('Add New Slide'),
    'edit_item' => __('Edit Slide'),
    'new_item' => __('New Slide'),
    'view_item' => __('View Slide'),
    'search_items' => __('Search Slide'),
    'not_found' =>  __('No Slide found'),
    'not_found_in_trash' => __('No Slide found in Trash'),
    'parent_item_colon' => ''
  );
  register_post_type(
    'slider',
    array(
      'labels' => $labels,
      'public' => true,
      'show_ui' => true,
      'hierarchical' => false,
      'rewrite' => true,
      'exclude_from_search' => true,
      'show_in_nav_menus' => false,
      'supports' => array(
        'title',
        'excerpt',
        'editor',
        'thumbnail',
        'page-attributes',
        'custom-fields',
        'comments',
      ),
    )
  );
  // Works
  $labels = array(
    'name' => _x('Works', 'post type general name'),
    'singular_name' => _x('works', 'post type singular name'),
    'add_new' => _x('Add New', 'faqs'),
    'add_new_item' => __('Add New Work'),
    'edit_item' => __('Edit Work'),
    'new_item' => __('New Work'),
    'view_item' => __('View Work'),
    'search_items' => __('Search Work'),
    'not_found' =>  __('No Work found'),
    'not_found_in_trash' => __('No Work found in Trash'),
    'parent_item_colon' => ''
  );
  register_post_type(
    'works',
    array(
      'labels' => $labels,
      'public' => true,
      'show_ui' => true,
      'hierarchical' => false,
      'rewrite' => true,
      'exclude_from_search' => true,
      'show_in_nav_menus' => false,
      'supports' => array(
        'title',
        'excerpt',
        'editor',
        'thumbnail',
        'page-attributes',
        'custom-fields',
        'comments',
      ),
    )
  );
  // banners
  $labels = array(
    'name' => _x('Banners', 'post type general name'),
    'singular_name' => _x('banners', 'post type singular name'),
    'add_new' => _x('Add New', 'faqs'),
    'add_new_item' => __('Add New Banner'),
    'edit_item' => __('Edit Banner'),
    'new_item' => __('New Banner'),
    'view_item' => __('View Banner'),
    'search_items' => __('Search Banner'),
    'not_found' =>  __('No Banner found'),
    'not_found_in_trash' => __('No Banner found in Trash'),
    'parent_item_colon' => ''
  );
  register_post_type(
    'banners',
    array(
      'labels' => $labels,
      'public' => true,
      'show_ui' => true,
      'hierarchical' => false,
      'rewrite' => true,
      'exclude_from_search' => true,
      'show_in_nav_menus' => false,
      'supports' => array(
        'title',
        'excerpt',
        'editor',
        'thumbnail',
        'page-attributes',
        'custom-fields',
        'comments',
      ),
    )
  );
  // include scripts
  $includes_path = TEMPLATEPATH . '/includes/';
  require_once $includes_path . 'theme-scripts.php';
  //
  class description_walker extends Walker_Nav_Menu
     {
      function start_el(&$output, $item, $depth, $args)
      {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        $class_names = $value = '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        if($depth != 0) {
          $classes[]='supersub';
        } else {
          $classes[]='superzero';
        }
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="'. esc_attr( $class_names ) . '"';
        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $prepend = '<span><b>';
        // <span class="arrowwrapper"><span class="arrow"></span></span>
        $append = '</b></span>';
        //$description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';
        if($depth != 0) {
          $description = $append = $prepend = "";
        }
        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
        $item_output .= $description.$args->link_after;
        $item_output .= '<span class="arrowwrapper"><span class="arrow"></span></span></a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
      }
}


class description_walker2 extends Walker_Nav_Menu
     {
      function start_el(&$output, $item, $depth, $args)
      {
     if ($depth!=1) {
             return;
           }
           global $wp_query;
           $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
           $class_names = $value = '';
           $classes = empty( $item->classes ) ? array() : (array) $item->classes;
           $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
           $class_names = ' class="'. esc_attr( $class_names ) . '"';
           $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
           $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
           $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
           $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
           $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
           $prepend = '<span><b>';
           $append = '</b></span>';
           //$description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';
           if($depth != 0)
           {
                     $description = $append = $prepend = "";
           }
            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>'.$depth;
            $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
            $item_output .= $description.$args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
            }
}
      // Display 20 products per page in woo commerce
      add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 20;' ), 20 );


add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment( $fragments ) {
  global $woocommerce;

  ob_start();

  ?>
  <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
  <?php

  $fragments['a.cart-contents'] = ob_get_clean();

  return $fragments;

}


add_filter('woocommerce_get_catalog_ordering_args', 'custom_woocommerce_get_catalog_ordering_args');

function custom_woocommerce_get_catalog_ordering_args( $args ) {
  $orderby_value = isset( $_GET['orderby'] ) ? woocommerce_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );

    if ('upc' == $orderby_value){
        $args['orderby'] = 'meta_value_num';
        $args['order'] = 'desc';
        $args['meta_key'] = 'order_pa_materials';
    }

  return $args;
}

add_filter( 'woocommerce_default_catalog_orderby_options', 'custom_woocommerce_catalog_orderby' );
add_filter( 'woocommerce_catalog_orderby', 'custom_woocommerce_catalog_orderby' );

function custom_woocommerce_catalog_orderby( $sortby ) {
  $sortby['upc'] = 'UPC Code';
  return $sortby;
}

function striposa($haystack, $needles=array(), $offset=0) {
    $chr = array();
    foreach($needles as $needle) {
        $res = strpos($haystack, $needle, $offset);
        if ($res !== false) $chr[$needle] = $res;
    }
    if(empty($chr)) return false;
    return min($chr);
}

/**
 * Open a preview e-mail.
 *
 * @return null
 */
function previewEmail() {

    if (is_admin()) {
        $default_path = WC()->plugin_path() . '/templates/';

        $files = scandir($default_path . 'emails');
        $exclude = array( '.', '..', 'email-header.php', 'email-footer.php','plain' );
        $list = array_diff($files,$exclude);
        ?><form method="get" action="<?php echo site_url(); ?>/wp-admin/admin-ajax.php">
<input type="hidden" name="order" value="2055">
<input type="hidden" name="action" value="previewemail">
        <select name="file">
        <?php
        foreach( $list as $item ){ ?>
            <option value="<?php echo $item; ?>"><?php echo str_replace('.php', '', $item); ?></option>
        <?php } ?>
        </select><input type="submit" value="Go"></form><?php
        global $order;
        $order = new WC_Order($_GET['order']);
        wc_get_template( 'emails/email-header.php', array( 'order' => $order ) );


        wc_get_template( 'emails/'.$_GET['file'], array( 'order' => $order ) );
        wc_get_template( 'emails/email-footer.php', array( 'order' => $order ) );

    }
    return null; 
}

add_action('wp_ajax_previewemail', 'previewEmail');




?>