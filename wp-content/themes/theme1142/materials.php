<?php
/*
Template Name: Materials 
*/
?>
<?php get_header(); ?>
<div id="primary">
   <div class="bot-indent">
      <div class="inside">
         <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
         <div id="post-<?php the_ID(); ?>" <?php post_class('page'); ?>>
            <article>
               <!-- <div class="title"><span class="fright edit"><?php edit_post_link('<small>Edit this entry</small>','',''); ?></span><h1><?php the_title(); ?></h1></div> -->
               <?php if ( has_post_thumbnail()) { echo '<div class="featuredThumbnail">'; the_post_thumbnail(); echo '</div>'; } ?>
               <!-- loades the post's featured thumbnail, requires Wordpress 3.0+ -->
               <div id="page-content">
                  <?php the_content(); ?>
                  <div class="pagination">
                     <?php wp_link_pages('before=<div class="pagination">&after=</div>'); ?>
                  </div>
                  <!--.pagination-->
               </div>
               <!--#pageContent -->
            </article>
	  </div>
         <!--#post-# .post-->
         <?php endwhile; ?>
      </div>
   </div>
</div>
<?php wp_nav_menu( array('theme_location'  => 'materials','menu' => 'materials')); ?>
<!--#content-->
<?php get_sidebar(); ?>

<?php get_footer(); ?>