<?php get_header(); ?>
<div id="primary">
	<div class="inside bot-indent1"><div id="error404" class="post">
		<br>
		<h1>Oops! This page cannot be found</h1>
		<table><tbody class="table"><tr><td width=940>
		<div class="postContent">
			<p>Unfortunately, we are unable to locate the page you are trying to access. We recently redesigned our website and page URLs have changed. Though we're excited for the new design, we are apologetic that it may have caused this error on your visit.  It's possible that the information you are looking for has been moved, so don't give up! <br><br>Please check the page URL or use the search field at top to resolve this issue. If you're looking for something specific or are unable to find the page your looking for, please contact customer service and we'll be happy to assist you.<br><h3>Don't Go! Try these Helpful Links & Bits of Information</h3>
<table width=910 class="nopad"><tbody><tr><td width=450>
<ul>
<li><a href="/about">About Kinco, Our Team, & Our Location</a></li>
<li><a href="/about/history/">About Our History, Bruce, Travis & the nitty-gritty details</a></li>
<li><a href="/about/brands/">View Kinco Registered &/or Trademarked Brands</a></li>
<li><a href="/about/giving/">Learn About How We Give Back</a></li>
<li><a href="/about/press-reviews/">Heard the news? Checkout Kinco in the Press & read product Reviews</a></li>
<li><a href="/glove-101/">Become more familiar Gloves in a Crash Course in Sizing, Materials, etc.</a></li>
</ul>
</td>
<td width=450>
<ul>
<li><a href="/products/">View our Products & filter by feature, material, use, design or size</a></li>
<li><a href="/products/download-catalog/">Request a copy of our latest Catalog</a></li>
<li><a href="/how-to-buy/">Want to Buy? Learn how to get your hands on (or IN!) our Gloves</a></li>
<li><a href="http://ski.kinco.com/">Work for a Ski Resort? We offer exclusive sales directly to you</a></li>
<li><a href="/resellers/">Resellers! View product lists, track orders, & access your account here! </a></li>
<li><a href="/contact-us/faqs/">Question? Checkout our FAQs</a></li>
</ul>
</td></tr></tbody></table>
We hope these links get you to the right place. If you still need help, please chat with our of our Glove Experts or send us a <a href="/contact-us">Contact Request</a>. Looking forward to doing business with you!</p>
		</div><!--.post-content--></td></tr></tbody></table>
	</div><!--#error404 .post--></div><br>
</div><!--#content-->
<?php get_footer(); ?>