<?php
/*
Template Name: Works
*/
?>
<?php get_header(); ?>
<div id="primary">
   <div class="inside bot-indent">
      <div class="title"><h1>
         <?php the_title(); ?>
      </h1></div>
	 <?php $wp_query = null; ?>
      <?php $wp_query = new WP_Query('post_type=works&posts_per_page=3&paged='.$paged ); ?>
      <div class="newer-older top">
         <div class="older">
            <p>
               <?php next_posts_link('&laquo; Older Entries') ?>
            </p>
         </div>
         <!--.older-->
         <div class="newer">
            <p>
               <?php previous_posts_link('Newer Entries &raquo;') ?>
            </p>
         </div>
         <!--.older-->
      </div>
      <!--.oldernewer-->
      <div id="post-<?php the_ID(); ?>" <?php post_class('works'); ?>>
         <ul class="posts-list">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <li>
               <div class="title"><h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
   </div>
               <?php if ( has_post_thumbnail()) { echo '<div class="featuredThumbnail">'; the_post_thumbnail(''); echo '</div>'; } ?>
               <!-- loades the post's featured thumbnail, requires Wordpress 3.0+ -->
               <p><span class="fright"><?php comments_popup_link('No comments', 'One comment', '% comments', 'comments-link', 'Comments are closed'); ?></span> Written on
                  <?php the_time('m.j.Y'); ?>
                  at
                  <?php the_time() ?>
                  , by
                  <?php the_author_posts_link() ?>
               </p>
               <div class="post-excerpt">
                  <?php the_excerpt(); ?>
                  <!-- the excerpt is loaded to help avoid duplicate content issues -->
               </div>
               <?php endwhile; else: ?>
               <div class="no-results">
                  <p><strong>There has been an error.</strong></p>
                  <p>We apologize for any inconvenience, please <a href="<?php bloginfo('url'); ?>/" title="<?php bloginfo('description'); ?>">return to the home page</a> or use the search form below.</p>
                  <?php get_search_form(); ?>
                  <!-- outputs the default Wordpress search form-->
               </div>
               <!--noResults-->
            </li>
            <?php endif; ?>
         </ul>
      </div>
      <div class="newer-older bot">
         <div class="older">
            <p>
               <?php next_posts_link('&laquo; Older Entries') ?>
            </p>
         </div>
         <!--.older-->
         <div class="newer">
            <p>
               <?php previous_posts_link('Newer Entries &raquo;') ?>
            </p>
         </div>
         <!--.older-->
      </div>
      <!--.oldernewer-->
   </div>
</div>
<!--#content-->
<?php get_sidebar(''); ?>
<?php get_footer(); ?>