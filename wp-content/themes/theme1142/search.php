<?php get_header(); ?>
<div id="primary" class="search">
   <div class="inside"><h1>
      Search Results for: <?php the_search_query(); ?>
   </h1>
   <ul class="posts-list">
   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   <li>
   <div class="title"><h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2><?php edit_post_link('<small>Edit this entry</small>','',''); ?>
   </div>
   <?php if ( has_post_thumbnail()) { echo '<div class="featuredThumbnail">'; the_post_thumbnail(); echo '</div>'; } ?>
   <!-- loades the post's featured thumbnail, requires Wordpress 3.0+ -->
   <div class="post-excerpt">
      <?php the_excerpt(); ?>
      <!-- the excerpt is loaded to help avoid duplicate content issues -->
   </div>
   <!--.post-excerpt-->
   </li>
   <?php endwhile; else: ?>
   <li><div class="no-results">
      <h2>No Results Found</h2>
      <p>We were unable to locate what you're looking for. Please attempt your search using a different query or term, and try again. Thank you!</p>
      <?php get_search_form(); ?>
      <!-- outputs the default Wordpress search form-->
   </div></li>
   <!--noResults-->
   <?php endif; ?>
   </ul>
   <nav class="oldernewer bot">
      <div class="older">
         <p>
            <?php next_posts_link('&laquo; Older Entries') ?>
         </p>
      </div>
      <!--.older-->
      <div class="newer">
         <p>
            <?php previous_posts_link('Newer Entries &raquo;') ?>
         </p>
      </div>
      <!--.older-->
   </nav>
   <!--.oldernewer--></div>
</div>
<!-- #content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>