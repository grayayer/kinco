<?php
/*
Template Name: Blog
*/
?>
<?php get_header(); ?>
<div id="primary" class="hfeed">
   <div class="inside"><?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
   <h1 class="title"><?php the_title(); ?></h1>
   <?php endwhile; ?>
   <?php $wp_query = null; ?>
   <?php $wp_query = new WP_Query('post_type=post&posts_per_page=5&paged='.$paged ); ?>
   <div class="newer-older top">
      <div class="older">
         <p>
            <?php next_posts_link('&laquo; Older Entries') ?>
         </p>
      </div>
      <!--.older-->
      <div class="newer">
         <p>
            <?php previous_posts_link('Newer Entries &raquo;') ?>
         </p>
      </div>
      <!--.older-->
   </div>
   <!--.oldernewer-->
   <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <ul class="posts-list">
         <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
         <li>
            <div class="title"><h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2><span class="fright"><?php comments_popup_link('No comments', '1 comment', '% comments', 'comments-link', 'Comments are closed'); ?></span>Written on
      <?php the_time('F j, Y'); ?>
      at
      <?php the_time() ?>
      , by
      <?php the_author_posts_link() ?>
   </div>
            <div class="entry-post-content">
               <div class="entry-content"> 
                  <?php if ( has_post_thumbnail()) { echo '<div class="featuredThumbnail">'; the_post_thumbnail(); echo '</div>'; } ?>
                  <?php the_excerpt(); ?>
               </div>
            </div>
            <!-- .entry-content -->
         </li>
         <?php endwhile; ?>
      </ul>
   </div>
   <div class="newer-older bot">
      <div class="older">
         <p>
            <?php next_posts_link('&laquo; Older Entries') ?>
         </p>
      </div>
      <!--.older-->
      <div class="newer">
         <p>
            <?php previous_posts_link('Newer Entries &raquo;') ?>
         </p>
      </div>
      <!--.older-->
   </div>
   <!--.oldernewer--></div>
</div>
<?php get_sidebar();?>
<?php get_footer(); ?>