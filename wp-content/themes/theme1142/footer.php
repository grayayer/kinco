</div>
</div>
<!--.container-->
<div class="clear"></div>
<div class="push"></div>
</div><!-- end .wrapper -->
<footer>
   <div class="container">
      <p class="inside"><span class="fright">
	<table width="940px">
	<tbody>
	<tr>
	<td style="text-align: left;" width="16%"><a class="footer-text" href="/about"><strong>About</strong></a><br>
	<a class="footer-text" href="/about/history">Our History</a><br>
	<a class="footer-text" href="/about/brands">Our Brands</a><br>
	<a class="footer-text" href="/about/social-responsibility">Social Responsibility</a><br>
	<a class="footer-text" href="/about/donations-sponsorship/">Donations & Sponsorship</a><br>
	<a class="footer-text" href="/about/press-reviews">Press & Reviews</a><br>
	<a class="footer-text" href="/about/submit">Submit Images & Reviews</a></td>
	<td style="text-align: left;" width="11%"><a class="footer-text" href="/glove-101"><strong>Glove 101</strong></a><br>
	<a class="footer-text" href="/glove-101/sizing">Sizing</a><br>
	<a class="footer-text" href="/glove-101/materials">Materials</a><br>
	<a class="footer-text" href="/glove-101/patterns">Patterns</a><br>
	<a class="footer-text" href="/glove-101/thumb-designs">Thumb Designs</a><br>
	<a class="footer-text" href="/glove-101/cuff-styles">Cuff Styles</a></td>
	<td style="text-align: left;" width="12%"><a class="footer-text" href="/products"><strong>Products</strong></a><br>
	<a class="footer-text" href="/product-category/gloves">Gloves</a><br>
	<a class="footer-text" href="/product-category/garments">Garments</a><br>
	<a class="footer-text" href="/product-category/displays">Displays</a><br>
	<a class="footer-text" href="/ski">Ski Program</a><br>
	<a class="footer-text" href="/products/download-catalog">Download Catalog</a></td>
	<td style="text-align: left;" width="14%"><a class="footer-text" href="/how-to-buy"><strong>How To Buy</strong></a><br>
	<td style="text-align: left;" width="15%"><a class="footer-text" href="/resellers/"><strong>Resellers</strong></a><br>
	<a class="footer-text" href="/resellers/track-your-order">Track Your Order</a><br>
	<a class="footer-text" href="/products/styles-upcs/">Styles & UPCs</a><br>
	<a class="footer-text" href="/resellers/submit-store-images/">Submit Store Images</a><br>
	<a class="footer-text" href="https://www.kincocustomer.com/request" target="_blank">New Reseller Account</a>
	</td></td>
	<td style="text-align: left;" width="12%"><strong><a class="footer-text" href="/contact-us">Contact Us</a></strong><br>
	<a class="footer-text" href="/contact-us/faqs/">FAQs</a><br>
	<a class="footer-text" href="/contact-us/warranty-information">Warranty Information</a><br>
	<a class="footer-text" href="/contact-us/privacy-policy">Privacy Policy</a><br>
	<a class="footer-text" href="/contact-us/terms-conditions">Terms & Conditions</a><br>
	<td style="text-align: right;" width=165>
<a href="https://www.facebook.com/KincoGloves" target="_blank"><img class="nohover" alt="Like us on Facebook" src="/wp-content/uploads/social_facebook_off.png" /><img class="hover" alt="Like us on Facebook" src="/wp-content/uploads/social_facebook_on.png" /></a> <a href="https://twitter.com/KincoGloves" target="_blank"><img class="nohover" alt="Follow us on Twitter" src="/wp-content/uploads/social_twitter_off.png" /><img class="hover" alt="Follow us on Twitter" src="/wp-content/uploads/social_twitter_on.png" /></a> 
<a class="" href="https://instagram.com/kincogloves/" target="_blank"><img class="nohover" alt=“Follow us on Instagram“ src="/wp-content/uploads/social_instagram_off.png" /><img class="hover" alt="Follow us on Instagram" src="/wp-content/uploads/social_instagram_on.png" /></a> <a class="" href="https://www.pinterest.com/kincogloves/" target="_blank"><img class="nohover" alt=“Find us on Pinterest“ src="/wp-content/uploads/social_pinterest_off.png" /><img class="hover" alt=“Find us on Pinterest" src="/wp-content/uploads/social_pinterest_on.png" /></a> <a class="" href="http://www.linkedin.com/company/kinco" target="_blank"><img class="nohover" alt="Join us on Linked In" src="/wp-content/uploads/social_linkedin_off.png" /><img class="hover" alt="Join us on Linked In" src="/wp-content/uploads/social_linkedin_on.png" /></a> <a class="" href="https://plus.google.com/109277835235781530546/about?gl=us&hl=en" target="_blank"><img class="nohover" alt="Join us on Google Plus" src="/wp-content/uploads/social_googleplus_off.png" /><img class="hover" alt="Join us on Google Plus" src="/wp-content/uploads/social_googleplus_on.png" /></a><br><br>
<span style="color:#686868; font-size:10px;">© 2018 Kinco, LLC<br>
Portland, Oregon 97230 USA<br>
All Rights Reserved</span></td>
</tr>
</tbody>
</table>

<!--
         <a href="<?php bloginfo('url'); ?>/"><img class="logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/footer-logo.png" /></a>
         is proudly powered by <a href="http://wordpress.org" class="wp-link">Wordpress</a><br />
         Entries <a href="<?php bloginfo('rss2_url'); ?>" rel="nofollow">(RSS)</a> Comments <a href="<?php bloginfo('comments_rss2_url'); ?>" rel="nofollow">(RSS)</a></span>
         Like this design? Browse for <a rel="nofollow" href="http://www.templatemonster.com/wordpress-themes.php" title="WordPress templates" target="_blank">more WordPress themes</a>!
-->

</p>
   </div>
   <!--.container-->
</footer>
<?php wp_footer(); ?>
<script type="text/javascript">

  jQuery(function(){
      jQuery(document).ready(function($) {
          $(".accordionButton").append('<img style="padding-top: 10px; padding-left: 15px;" width=9 height=14 src="/wp-content/themes/theme1142/images/arrow14.png">');
          $(".widget_woocommerce_product_filter_attribute ul li a").each(function(index,value) {
              console.log("a "+index+" :"+$(this).attr('data-key')+'='+$(this).attr('data-value'));
              $(this).click(function() {
                  $(this).closest(".accordionContent").attr('style','display: none');
              });
          });
          $(document).on('change','#pa_sizes',function() {
              if ($(".single_variation span.price").length) {
                  //alert("special pricing");
                  $(".variations_form div .price .amount").attr('style','display: none !important');
                  $(".single_variation").attr('style','display: inline !important; float: left; margin-left: -97px;');
                  $(".single_variation .price .amount").attr('style','display: inline');
                  //$(".variations_button").attr('style',' margin-top: -85px; margin-left: 100px; float: right;');
                  $(".variations_button").attr('style','position: absolute; left: 164px; top: 68px');
              }
          });
      });
  });
</script>


<!-- this is used by many Wordpress features and for plugins to work proporly -->
</body></html>