<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<title>
<?php if ( is_tag() ) {
      echo 'Tag Archive for &quot;'.$tag.'&quot; ';
    } elseif ( is_archive() ) {
      wp_title(); echo ' ';
    } elseif ( is_search() ) {
      echo 'Search Results for &quot;'.wp_specialchars($s).'&quot; ';
    } elseif ( is_home() ) {
      bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
    }  elseif( is_404() ) {
      echo 'Error 404 Not Found ';
    } else {
      echo wp_title( ' | ', false, right );
      // Ben commented this part out, it was adding the sitename after most every page
      // bloginfo( 'name' );
    } ?>
</title>
<meta name="keywords" content="<?php wp_title(); echo ' , '; bloginfo( 'name' ); echo ' , '; bloginfo( 'description' ); ?>" />


<meta charset="<?php bloginfo( 'charset' ); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="index" title="<?php bloginfo( 'name' ); ?>" href="<?php echo get_option('home'); ?>/" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'rss2_url' ); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'atom_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/print.css" type="text/css" media="print" />
<?php
  /* We added some JavaScript to pages with the comment form
   * to support sites with threaded comments (when in use).
   */
  if ( is_singular() && get_option( 'thread_comments' ) )
   wp_enqueue_script( 'comment-reply' );

  /* Always have wp_head() just before the closing </head>
   * tag of your theme, or you will break many plugins, which
   * generally use this hook to add elements to <head> such
   * as styles, scripts, and meta tags.
   */
  wp_head();
?>
<!-- The HTML5 Shim is required for older browsers, mainly older versions IE -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if lt IE 7]>
  <script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script.js"></script>
<![endif]-->
<script type="text/javascript">
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
      $j('.sf-menu ul').superfish({
        delay:       1000,
        animation:   {opacity:'show',height:'show'},
        speed:       'fast',
        autoArrows:  false,
        dropShadows: false
      });
      $j("#faded").faded({
        speed: 500,
        autoplay: 5000
      });
      $j(".carousel").jCarouselLite({
        btnNext: ".next-button",
        btnPrev: ".prev-button",
        visible: 3,
        auto: 7000
      });
    });
</script>
<script type='text/javascript' src='/wp-content/themes/theme1142/js/accordian.js'></script>
<!-- Pinterest Site Verification -->
<meta name="p:domain_verify" content="5c0be542a94b2102b05b75862258047a"/>
</head>
<body <?php body_class(); ?>>
<!-- this encompasses the entire Web site -->
<div class="wrapper">
<header>
   <div class="splash">
      <div class="container">
         <div id="logo">
            <?php if( is_front_page() || is_home() ) { ?>
            <h1><a href="<?php bloginfo('url'); ?>/" title="<?php bloginfo('description'); ?>">
               <span><?php bloginfo('name'); ?></span>
               </a></h1>
            <?php } else { ?>
            <h2><a href="<?php bloginfo('url'); ?>/" title="<?php bloginfo('description'); ?>">
               <span><?php bloginfo('name'); ?></span>
               </a></h2>
            <?php } ?>
            <span id="description">
            <?php bloginfo('description'); ?>
            </span>
          </div><!--.logo-->
      <nav class="primary sf-menu">
        <?php wp_nav_menu( array(
          // WP4.0:
          'theme_location'=>'header_menu',
          // was in WP3.5:
          //'menu' => 'Header Menu',
          'walker' => new description_walker()
        )); ?>
        <!-- editable within the Wordpress backend -->
     </nav>
         <!--.primary-->
         <ul class="widgets-list">
            <?php if ( ! dynamic_sidebar( 'Header' ) ) : ?>
            <!-- Wigitized Header -->
            <?php endif ?>
         </ul>
<?php global $woocommerce;  if ( is_user_logged_in()) :?>
              <div id="minicart">
                <div class="logout">
                <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a> &nbsp;|&nbsp; <a href="<?php echo wp_logout_url( home_url() ); ?>">Log Out</a>
                </div>
        <div class="minicart-checkout">
                <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
                <?php if (sizeof($woocommerce->cart->cart_contents)>0) :?> &nbsp; &nbsp;
<a href="<?php echo $woocommerce->cart->get_checkout_url()?>" title="<?php _e('Checkout','woothemes') ?>" class="minicheckout"><?php _e('Checkout','woothemes') ?></a>
<?php endif; ?>
        </div>
                </div>
      <?php endif; ?>
      </div>
   </div>
   <!--.container-->
</header>
<div class="clear"></div>
<div class="cont-bg">
<div class="container">