<?php get_header(); ?>
<div id="primary">
   <div class="bot-indent">
      <div id="faded">
         <?php $loop = new WP_Query(array('post_type' => 'slider', 'posts_per_page' => 50)); ?>
         <?php if ($loop->have_posts()): ?>
         <ul class="slides">
            <?php while ($loop->have_posts()) : $loop->the_post(); $posts_counter++; ?>
            <li><?php $postid = get_the_ID(); ?>
               <a href="<?php echo get_post_meta($postid, "slider-url", $single = true); ?>"><?php if ( has_post_thumbnail()) { echo '<div class="featuredThumbnail">'; the_post_thumbnail(''); echo '</div>'; } ?></a>
            </li>
            <?php endwhile; ?>
         </ul>
         <?php endif; ?>
         <?php wp_reset_query(); ?>
         <a href="#" class="prev">prev</a> <a href="#" class="next">next</a></div>
      <div class="inside">
         <?php if ( ! dynamic_sidebar( 'Text sidebar' )) : ?>
         <!-- Wigitized Sidebar -->
         <?php endif; ?>
      </div>
      <div class="carousel-box">
         <?php $loop = new WP_Query(array('post_type' => 'works', 'posts_per_page' => 50)); ?>
         <?php if ($loop->have_posts()): ?>
         <a href="#" class="prev-button"></a><a href="#" class="next-button"></a>
         <div class="carousel">
            <ul>
               <?php while ($loop->have_posts()) : $loop->the_post(); $posts_counter++; ?>
			
			<?php 
									$custom = get_post_custom($post->ID);
									$address = $custom["address"][0];
									$category = $custom["category"][0];
									$sliderurl = $custom["slider-url"][0];
									?>
			
               <li><?php $postid = get_the_ID(); ?>
			    <h3><?php echo $category; ?></h3>
			    
			    <a href="http://<?php echo $address; ?>" class="address"><?php echo $address; ?></a>
			    
			    <?php if ($sliderurl != "") { ?>	
					<a href="http://<?php echo $sliderurl; ?>"><?php if ( has_post_thumbnail()) { echo '<div class="featuredThumbnail">'; the_post_thumbnail(); echo '</div>'; } ?></a>
			    <?php } else { ?>	 	
			    		<a href="<?php bloginfo('url'); ?>/?page_id=11"><?php if ( has_post_thumbnail()) { echo '<div class="featuredThumbnail">'; the_post_thumbnail(); echo '</div>'; } ?></a>
			    <?php } ?>
			    
			    <?php the_content('more info'); ?>
               </li>
               <?php endwhile; ?>
            </ul>
         </div>
         <?php endif; ?>
         <?php wp_reset_query(); ?>
      </div>
	 <div class="inside">
	    <div class="line-hor1"></div>
	    <div id="banners">
         <?php $loop = new WP_Query(array('post_type' => 'banners', 'posts_per_page' => 6)); ?>
         <?php if ($loop->have_posts()): ?>
         <ul>
	    	  <?php $posts_counter = 0; ?>
            <?php while ($loop->have_posts()) : $loop->the_post(); $posts_counter++; ?>
            <li class="banner banner-<?php echo $posts_counter; ?>">
               <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			<?php the_excerpt(); ?>
            </li>
            <?php endwhile; ?>
         </ul>
         <?php endif; ?>
         <?php wp_reset_query(); ?>
   	</div></div>
   </div>
</div>
<!--#content-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>