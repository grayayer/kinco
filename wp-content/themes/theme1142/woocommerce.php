<?php get_header();

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );

?>

<div id="primary">
   <div class="bot-indent">
      <div class="inside">
         <?php //woocommerce_content(); 
         
         ?>
         
         <?
         if ( is_singular( 'product' ) ) {

							while ( have_posts() ) : the_post();
				
								wc_get_template_part( 'content', 'single-product' );
				
							endwhile;
				
						} else { ?>
				
							<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				
								<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
				
							<?php endif; ?>
				
							<?php do_action( 'woocommerce_archive_description' );
							woocommerce_get_sidebar();
							 ?>
				
							<?php if ( have_posts() ) : ?>
				
								<?php do_action('woocommerce_before_shop_loop'); ?>
				
								<?php woocommerce_product_loop_start(); ?>
				
									<?php woocommerce_product_subcategories(); ?>
				
									<?php while ( have_posts() ) : the_post(); ?>
				
										<?php wc_get_template_part( 'content', 'product' ); ?>
				
									<?php endwhile; // end of the loop. ?>
				
								<?php woocommerce_product_loop_end(); ?>
				
								<?php do_action('woocommerce_after_shop_loop'); ?>
				
							<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
				
								<?php wc_get_template( 'loop/no-products-found.php' ); ?>
				
							<?php endif;
				
						}
         ?>
         
      </div>
   </div>
</div>
<!--#content-->
<!-- woo side -->
<?php
//// 3rd level nav
//
//global $wp_query;
//$cat_obj = $wp_query->get_queried_object();
//
//if (stripos($cat_obj->description,'menu-3rd-nav-our-brands-container')===false) {
//  wp_nav_menu( array('theme_location'  => 'prod_detail','menu' => 'prod_detail'));
//}
//?>
<?php
// woocommerce_get_sidebar();
?>

<!-- regular side -->
<!-- // sort by -->

<?php get_footer(); ?>